﻿namespace BuySell.Domain
{

    public class JsonPuzzleData
    {
        public string puzzle { get; set; }
        public string info { get; set; }
        public string submission { get; set; }
        public DailyIndexData[] data { get; set; }
    }

    public class DailyIndexData
    {
        public int quote_date { get; set; }
        public string paper { get; set; }
        public string exch { get; set; }
        public float open { get; set; }
        public float high { get; set; }
        public float low { get; set; }
        public float close { get; set; }
        public int volume { get; set; }
        public int value { get; set; }
    }
}
