﻿namespace BuySell.Domain
{
    public class BuySellPair
    {
        public DailyIndexData BuyIndexData { get; }
        public DailyIndexData SellIndexData { get; }
        public BuySellPair(DailyIndexData buyIndexData, DailyIndexData sellIndexData)
        {
            BuyIndexData = buyIndexData;
            SellIndexData = sellIndexData;
        }
    }
}
