﻿using System;
using System.Collections.Generic;
using System.Linq;
using BuySell.Domain;

namespace BuySell
{
    public static class BuySellCalculator
    {
        public static BuySellPair GetBestBuySell(IEnumerable<DailyIndexData> buySellData)
        {
            var ordered = buySellData.ToList();

            DailyIndexData previousBuy, previousSell, tempBuy, tempSell;
            previousBuy = previousSell = tempBuy = tempSell = ordered.First();

            foreach (var currentDayPrice in ordered.Skip(1))
            {
                if (currentDayPrice.low < previousBuy.low || currentDayPrice.low < tempBuy.low)
                {
                    tempBuy = currentDayPrice;
                }

                if (currentDayPrice.high > previousSell.high && currentDayPrice.high > tempSell.high)
                {
                    tempSell = currentDayPrice;
                    tempBuy = currentDayPrice;
                }

                if (GetProfit(tempBuy, tempSell) >= GetProfit(previousBuy, previousSell))
                {
                    previousBuy = tempBuy;
                    previousSell = tempSell;
                }
            }

            return new BuySellPair(previousBuy, previousSell);
        }

        private static float GetProfit(DailyIndexData buy, DailyIndexData sell)
        {
            return sell.high - buy.low;
        }
    }


}
