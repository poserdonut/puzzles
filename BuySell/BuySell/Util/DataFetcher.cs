﻿using System.Net.Http;
using BuySell.Domain;
using Newtonsoft.Json;

namespace BuySell.Util
{
    public static class DataFetcher
    {
        public static JsonPuzzleData GetData()
        {
            var client = new HttpClient();

            var json = client.GetStringAsync("https://modularfinance.se/api/puzzles/index-trader.json");

            var puzzleData = JsonConvert.DeserializeObject<JsonPuzzleData>(json.Result);

            return puzzleData;
        }
    }
}
