﻿using System;
using BuySell.Util;

namespace BuySell
{
    public static class Program
    {
        static void Main(string[] args)
        {
            var puzzle = DataFetcher.GetData();

            var buySellPair = BuySellCalculator.GetBestBuySell(puzzle.data);
            Console.WriteLine($"Date to buy: {buySellPair.BuyIndexData.quote_date} At price: {buySellPair.BuyIndexData.low}");
            Console.WriteLine($"Date to sell: {buySellPair.SellIndexData.quote_date} At price: {buySellPair.SellIndexData.high}");
            Console.WriteLine($"Profit: {buySellPair.SellIndexData.high - buySellPair.BuyIndexData.low}");

            Console.ReadLine();
        }
    }
}
