using System.Collections.Generic;
using System.IO;
using System.Linq;
using BuySell.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace BuySell.Test
{
    [TestClass]
    public class BuySellCalculatorTest
    {
        [TestMethod]
        public void GetBestBuySellFourthBuyFourthSell()
        {
            var testData = new[]
            {
                CreateDailyIndexData(1, 20, 20170905),
                CreateDailyIndexData(2, 11, 20171016),
                CreateDailyIndexData(1, 20, 20171022),
                CreateDailyIndexData(1, 22, 20171106),
                CreateDailyIndexData(2, 21, 20171224)
            };

            var buySellPair = BuySellCalculator.GetBestBuySell(OrderData(testData));

            AssertValidDates(buySellPair.BuyIndexData, buySellPair.SellIndexData);
            Assert.AreEqual(21, GetProfit(buySellPair.BuyIndexData, buySellPair.SellIndexData));
            AssertDailyIndexData(testData[3], buySellPair.BuyIndexData);
            AssertDailyIndexData(testData[3], buySellPair.SellIndexData);
        }

        [TestMethod]
        public void GetBestBuySellSecondBuyFourthSell()
        {
            var testData = new[]
            {
                CreateDailyIndexData(5, 20, 20170905),
                CreateDailyIndexData(3, 11, 20171016),
                CreateDailyIndexData(4, 20, 20171022),
                CreateDailyIndexData(5, 22, 20171106),
                CreateDailyIndexData(6, 21, 20171224)
            };

            var buySellPair = BuySellCalculator.GetBestBuySell(OrderData(testData));

            AssertValidDates(buySellPair.BuyIndexData, buySellPair.SellIndexData);
            Assert.AreEqual(19, GetProfit(buySellPair.BuyIndexData, buySellPair.SellIndexData));
            AssertDailyIndexData(testData[1], buySellPair.BuyIndexData);
            AssertDailyIndexData(testData[3], buySellPair.SellIndexData);
        }

        [TestMethod]
        public void GetBestBuySellFifthBuyFifthSell()
        {
            var testData = new[]
            {
                CreateDailyIndexData(5, 20, 20170905),
                CreateDailyIndexData(3, 11, 20171016),
                CreateDailyIndexData(4, 20, 20171022),
                CreateDailyIndexData(5, 22, 20171106),
                CreateDailyIndexData(1, 21, 20171224)
            };

            var buySellPair = BuySellCalculator.GetBestBuySell(OrderData(testData));

            AssertValidDates(buySellPair.BuyIndexData, buySellPair.SellIndexData);
            Assert.AreEqual(20, GetProfit(buySellPair.BuyIndexData, buySellPair.SellIndexData));
            AssertDailyIndexData(testData[4], buySellPair.BuyIndexData);
            AssertDailyIndexData(testData[4], buySellPair.SellIndexData);
        }

        [TestMethod]
        public void GetBestBuySellFirstBuyFirstSell()
        {
            var testData = new[]
            {
                CreateDailyIndexData(2, 20, 20170905),
                CreateDailyIndexData(3, 6, 20171016),
                CreateDailyIndexData(1, 5, 20171022),
                CreateDailyIndexData(5, 15, 20171106),
                CreateDailyIndexData(1, 18, 20171224)
            };

            var buySellPair = BuySellCalculator.GetBestBuySell(OrderData(testData));

            AssertValidDates(buySellPair.BuyIndexData, buySellPair.SellIndexData);
            Assert.AreEqual(18, GetProfit(buySellPair.BuyIndexData, buySellPair.SellIndexData));
            AssertDailyIndexData(testData[0], buySellPair.BuyIndexData);
            AssertDailyIndexData(testData[0], buySellPair.SellIndexData);
        }

        [TestMethod]
        public void GetBestBuySellFromPuzzleData()
        {
            var jsonFromFile = File.ReadAllText("TestData\\index-trader.json");

            var testData = JsonConvert.DeserializeObject<JsonPuzzleData>(jsonFromFile).data;

            var buySellPair = BuySellCalculator.GetBestBuySell(testData);

            AssertValidDates(buySellPair.BuyIndexData, buySellPair.SellIndexData);
            Assert.AreEqual(1240.68, buySellPair.BuyIndexData.low, 0.0001);
            Assert.AreEqual(20160627, buySellPair.BuyIndexData.quote_date);

            Assert.AreEqual(1468.16, buySellPair.SellIndexData.high, 0.0001);
            Assert.AreEqual(20161011, buySellPair.SellIndexData.quote_date);
        }

        private static DailyIndexData CreateDailyIndexData(float low, float high, int date)
        {
            return new DailyIndexData
            {
                low = low,
                high = high,
                quote_date = date
            };
        }

        private static IEnumerable<DailyIndexData> OrderData(IEnumerable<DailyIndexData> testData)
        {
            return testData.OrderByDescending(x => x.quote_date);
        }

        private static float GetProfit(DailyIndexData buy, DailyIndexData sell)
        {
            return sell.high - buy.low;
        }

        private static void AssertDailyIndexData(DailyIndexData expected, DailyIndexData actual)
        {
            Assert.AreEqual(expected.quote_date, actual.quote_date);
            Assert.AreEqual(expected.low, actual.low);
            Assert.AreEqual(expected.high, actual.high);
        }

        private static void AssertValidDates(DailyIndexData buyData, DailyIndexData sellData)
        {
            Assert.IsTrue(buyData.quote_date <= sellData.quote_date);
        }
    }
}
