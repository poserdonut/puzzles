﻿using System;
using CurrencyAggregator.Util.Heap;

/*
 * Based on https://stackoverflow.com/a/34794716
 */
namespace CurrencyAggregator
{
    public class MedianFinder
    {
        private readonly MinHeap<double> _minHeap = new MinHeap<double>();
        private readonly MaxHeap<double> _maxHeap = new MaxHeap<double>();

        public void Insert(double i)
        {
            if (IsEmpty()) { _minHeap.Add(i); }
            else
            {
                if (i.CompareTo(GetMedian()) <= 0) { _maxHeap.Add(i); }
                else { _minHeap.Add(i); }
            }

            Balance();
        }

        private void Balance()
        {
            if (Math.Abs(_maxHeap.Count - _minHeap.Count) > 1)
            {
                if (_maxHeap.Count > _minHeap.Count)
                {
                    _minHeap.Add(_maxHeap.ExtractMax());
                }
                else { _maxHeap.Add(_minHeap.ExtractMin()); }
            }
        }

        public double GetMedian()
        {
            if (_maxHeap.Count == _minHeap.Count)
            {
                return (_maxHeap.Max + _minHeap.Min) / 2;
            }

            if (_maxHeap.Count > _minHeap.Count)
            {
                return _maxHeap.Max;
            }

            return _minHeap.Min;
        }

        private bool IsEmpty()
        {
            return _maxHeap.Count == 0 && _minHeap.Count == 0;
        }
    }
}
