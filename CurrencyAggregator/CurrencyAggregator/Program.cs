﻿using System;
using CurrencyAggregator.Util;

namespace CurrencyAggregator
{
    public class Program
    {
        static void Main(string[] args)
        {
            var currencyData = DataFetcher.GetData();
            var medianFinder = new MedianFinder();

            foreach (var data in currencyData)
            {
                medianFinder.Insert(data.rates.SEK);
                Console.WriteLine(medianFinder.GetMedian());
            }

            Console.WriteLine($"The Answer is {medianFinder.GetMedian()}");
            Console.ReadLine();
        }
    }
}
