﻿namespace CurrencyAggregator.Domain
{
    public class CurrencyData
    {
        public string date { get; set; }
        public Rates rates { get; set; }
    }

    public class Rates
    {
        public double SEK { get; set; }
    }
}