﻿using System.Net.Http;
using CurrencyAggregator.Domain;
using Newtonsoft.Json;

namespace CurrencyAggregator.Util
{
    public static class DataFetcher
    {
        public static CurrencyData[] GetData()
        {
            var client = new HttpClient();

            var json = client.GetStringAsync("http://fx.modfin.se/2017-01-01/2017-12-31?symbols=usd,sek&base=usd").Result;

            var currenyData = JsonConvert.DeserializeObject<CurrencyData[]>(json);

            return currenyData;
        }
    }
}
