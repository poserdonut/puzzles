using System.IO;
using CurrencyAggregator.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace CurrencyAggregator.Test
{
    [TestClass]
    public class MedianFinderTest
    {
        private MedianFinder _medianFinder;

        [TestInitialize]
        public void Init()
        {
            _medianFinder = new MedianFinder();
        }

        [TestMethod]
        public void GetMedian()
        {
            _medianFinder.Insert(2);
            Assert.AreEqual(2, _medianFinder.GetMedian());

            _medianFinder.Insert(1);
            Assert.AreEqual(1.5, _medianFinder.GetMedian());

            _medianFinder.Insert(4);
            Assert.AreEqual(2, _medianFinder.GetMedian());

            _medianFinder.Insert(3);
            Assert.AreEqual(2.5, _medianFinder.GetMedian());
        }

        [TestMethod]
        public void GetMedianFromJsonFile()
        {
            var currencyData = GetCurrencyDataFromJsonFile();

            foreach (var data in currencyData)
            {
                _medianFinder.Insert(data.rates.SEK);
            }

            Assert.AreEqual(8.429579647, _medianFinder.GetMedian(), 0.000000001);
        }

        private CurrencyData[] GetCurrencyDataFromJsonFile()
        {
            var json = File.ReadAllText("Testdata\\currencydata.json");

            var currencyData = JsonConvert.DeserializeObject<CurrencyData[]>(json);

            return currencyData;
        }
    }
}
